defmodule InstrumentStore.Repo.Migrations.CreateGuitars do
  use Ecto.Migration

  def change do
    create table(:guitars) do
      add :model, :string
      add :year, :integer
      add :is_available, :boolean, default: true, null: false

      timestamps()
    end
  end
end
